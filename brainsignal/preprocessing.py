import numpy as np
import scipy


def resampling(spantime, signal, new_spantime, window_length_time, polyorder=2):
    """ Resample signal from new timespan using first smoothing 
            and then interpolation.

        Parameters:
            spantime (np.array): array with timepoints [sec]
            signal (np.array): the signal at the timepoints
            new_spantime (np.array): new array with timepoints [sec]
            window_length_time (int or float): time length for smoothing
            polyorder (int): order of the polynomial to fit the date 
                            in the window_length time

        Returns:
            new_signal (np.array): signal at the timepoints in new_spantime.
    """
    dt = spantime[1] - spantime[0]
    n_points_in_time_window = round(window_length_time/dt)
    smoothed_signal = scipy.signal.savgol_filter(signal, n_points_in_time_window, polyorder)
    new_signal = np.interp(new_spantime, spantime, smoothed_signal)
    return new_signal


def standardize(signal):
    """ Standardized signal.

        Parameters:
            signal (np.array): Signal to be standardized

        Returns:
            standardized_signal (np.array): The standardized signal
    """
    standardized_signal = (signal - np.mean(signal))/np.std(signal)
    return standardized_signal