# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 2021

@author: Hamon
"""

#import 
import numpy as np 
import brainsignal.datanalysis as da
from scipy.stats import zscore
from scipy.interpolate import InterpolatedUnivariateSpline

def correction (signal, size_window, replace ,n):
    """
    Corrects outliers of a signal 

    Parameters
    ----------
    signal : np.array
        signal to correct.
    size_window : float
        choice of the sliding window.
    replace : method
        choice of the value for replacing the outlier (np.mean, np.median, np.nan).
    n : float
        number of standard deviation.

    Returns
    -------
    res_signal : np.array
        signal with outliers corrected.
    counter : int 
        number of replace values.
    index_corrected : 
        index of corrected values.
    
    """
    fs=200 #Hz
    counter=0
    window=int(size_window*fs)
    res_signal=[]
    index_corrected=[]
    i=0
    while i < np.size(signal)-1:
        signal_corrected=signal[i:i+window-1].copy() #copy of the signal to correct it
        cond=np.where(np.abs(zscore(signal[i:i+window-1]))> n)[0]
        counter=counter+len(cond)
        signal_corrected[np.abs(zscore(signal[i:i+window-1]))> n]= replace(signal[i:i+window-1])
        index_corrected.append(cond+i)
        res_signal.append(signal_corrected)
        i=i+window

    
    return res_signal, counter, index_corrected