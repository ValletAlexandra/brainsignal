# -*- coding: utf-8 -*-
"""
Created on Mon Jun 28 2021

@author: Hamon
"""
# import 
# Allow display of images
from IPython.display import display, Image

# Import the required modules
import numpy as np
import matplotlib.pyplot as plt
import glob
import scipy as sp

import pandas as pd
import seaborn as sns

# Tools from brainsignal library
import os
import sys
path=r'C:\Users\Hamon\Documents\Stage\brainsignal'
sys.path.append(path)
import brainsignal.datageneration as dg
import brainsignal.datanalysis as da
import brainsignal.HRVanalysis as ha
import brainsignal.bodediagram as bd



# Tools from sippy library
#from __future__ import division
from past.utils import old_div
from sippy import functionset as fset
from sippy import *
from sippy import system_identification 
import control.matlab as cnt
from matplotlib import rcParams
from scipy import signal as sg
from scipy import fftpack
import control 



def tf_armax(inpt, outpt, times, size_window,overlap=0):
    """
    Return array of transfer function corresponding to the window.

    Parameters
    ----------
    input : np.array
        input signal.
    output : np.array
        output signal.
    size_window : float
        duration of the sliding window.
    overlap : float 
        time of overlap.
    Returns
    -------
    omega: np.array
        frequency sample.
    dB_res: np.array
        magnitude.
    phase_res: np.array
        phase.
    time: np.array 
        time sample.

    """
    dB_res=[] #array to store magnitude
    phase_res=[] #array to store phase
    time=[] #second
    ti=30

    while ti < times[-1]-size_window:
        indices=da.indexseq(ti,size_window,times-1)

        #system identification 
        Id_sys = system_identification(outpt[indices], inpt[indices], 'ARMAX', IC='AIC',ARMAX_orders = [2,2,2,0],na_ord=[1,3],nb_ord=[1,2],nc_ord=[1,2],delays=[0,3])
        #bode 
        mag,phase,omega=control.bode(Id_sys.G)
        
        if np.size(dB_res)==0:
            dB_res=mag
            phase_res=phase
        else:
            dB_res=np.c_[dB_res,mag]
            phase_res=np.c_[phase_res,phase]
        
        time.append(ti+size_window/2)
        ti=ti+size_window-overlap
    
    return omega,dB_res, phase_res, time


def tf_crosspower(inpt, outpt,times,size_window=300,window=sg.windows.hann, overlap=0):
    """
    Calculate bode for all windows with cross power method 

    Parameters
    ----------
    inpt : np.array
        input signal.
    outpt : np.array
        output signal.
    size_window : float
        size of time window.
    times : np.array
        vector of time.
    window : np.array
        time window.
    overlap : float
        overlap time in seconds.

    Returns
    -------
    f_Pyx : np.array
        frequency sample.
    dB_mag : np.array
        magnitude corresponding to frequencies.
    phase : np.array
        phase corresponding to frequencies.
    time : np.array
        time sample.

    """
    
    fs=200 #Hz
    dB_mag=[] #list to store magnitude
    phase=[] #array to store phase
    time=[] #hour
    ti=30

    while ti < times[-1]-size_window:
        indices=da.indexseq(ti,size_window,times-1)
        x=inpt[indices]-np.mean(inpt[indices])
        y=outpt[indices]-np.mean(outpt[indices])
        f_Pyx, Pyx =sp.signal.csd(x,y, fs=fs, window='hann',nperseg=1024)
        f_Pxx, Pxx =sp.signal.csd(x, x, fs=fs, window='hann',nperseg=1024)

        # remove the 0 freq
        f_Pyx=f_Pyx[1::]
        Pyx=Pyx[1::]
        Pxx=Pxx[1::]

        TFPyx=Pyx/Pxx
    
        dB_mag_Pyx ,phase_Pyx = bd.find_dB_mag_and_phase(TFPyx)
        if np.size(dB_mag)==0:
            dB_mag=dB_mag_Pyx
            phase=phase_Pyx
        else:
            dB_mag=np.c_[dB_mag,dB_mag_Pyx]
            phase=np.c_[phase,phase_Pyx]
        
        time.append(ti+size_window/2)
        ti=ti+size_window-overlap
        
        
    return f_Pyx,dB_mag,phase,time



def tf_fft(inpt, outpt, times,size_window, window=sp.signal.windows.hann, overlap=0):
    """
    Calculate bode for all time windows using fft method.

    Parameters
    ----------
    inpt : np.array
        input signal.
    outpt : np.array
        output signal.
    size_window : float
        size of time window.
    times : np.array
        vector of time
    window : np.array
        time window.
    overlap : float
        overlap time in seconds.

    Returns
    -------
    sample_freq : np.array
        frequency sample.
    dB_res : np.array
        magnitude corresponding to frequencies.
    phase_res : np.array
        phase corresponding to frequencies.
    time : np.array
        time sample.

    """

    fs=200 #Hz
    dB_res=[] #array to store magnitude
    phase_res=[] #array to store phase
    time=[] #second
    ti=times[0]
    window=sp.signal.windows.hann

    while ti < times[-1]-size_window:
        indices=da.indexseq(ti,size_window,times-1)

        # fft 
        window=sp.signal.windows.flattop 
        window=sp.signal.windows.hann

        #input 
        X=fftpack.fft(inpt[indices]*window(len(inpt[indices]))) 

        #output 
        Y=fftpack.fft(outpt[indices]*window(len(outpt[indices])))

        #transfer function 
        H=Y/X
        
        # The corresponding frequencies
        sample_freq = fftpack.fftfreq(inpt[indices].size, d=times[1]-times[0])

        dB_mag ,phase = bd.find_dB_mag_and_phase(H)
        
        if np.size(dB_res)==0:
            dB_res=dB_mag
            phase_res=phase
        else:
            dB_res=np.c_[dB_res,dB_mag]
            phase_res=np.c_[phase_res,phase]

        ti=ti+size_window-overlap
        time.append((ti+size_window/2)/3600)
        
    return sample_freq, dB_res,phase_res, time

def ABP_ICP_features (ABP, ICP, method):
    """
    Calculate features common to ABP and ICP signals.

    Parameters
    ----------
    ABP : np.array
        ABP signal.
    ICP : np.array
        ICP signal.
    method : method
        choice of the method used for the correlation calculation.

    Returns
    -------
    r : float
        correlation founded between both signals.
    p : float
        p_value of correlation.
    cpp : float
        mean of difference between ABP and ICP.
    cpp_std : float
        standard deviation of ABP and ICP difference.

    """

    r, p = method(ABP, ICP)
    cpp=np.mean(ABP-ICP)
    cpp_std=np.std(ABP-ICP)
    
    
    return r,p, cpp, cpp_std

def PRx(time,abp,icp, method):
    """
    Calculate the pressure reactivity index between averaged ABP and averaged ICP on 5sec windows.

    Parameters
    ----------
    time : np.array
        time corresponding to the signal
    abp : np.array
        ABP signal.
    icp : np.array
        ICP signal.
    method : function
        method calculation of correlation.

    Returns
    -------
    PRx : np.array
        correlation between averaged ABP and averaged ICP.
    pvalue : np.array
        p-value of PRx

    """
    size_window=5 #seconds
    abp_mean=[]
    icp_mean=[] 
    ti=time[0]
    while ti < (time[len(time)-1]-size_window):
        indices=da.indexseq(ti,size_window,time)
        abp_mean.append(np.mean(abp[indices]))    
        icp_mean.append(np.mean(icp[indices]))
        ti=ti+size_window
    PRx,pvalue=method(abp_mean,icp_mean)
    
    
    return PRx, pvalue