######################################
### Module to generate synthetic data
######################################


#created by Alice Hamon 

# Needed libraries
import numpy as np
### time discretisation
def generate_time(duration,samplingrate,start=0):
    """ Generate the time points 

        Parameters:
            duration (float): Duration of the signal
            samplingrate (float) : Sampling frequency
            start (float): beginning time

        Returns:
            spantime (np.array): Array of times
    """
    dt=1/samplingrate
    Nt=int(duration*samplingrate)
    spantime=np.linspace(start,Nt*dt+start,Nt+1)
    return spantime

### Simple sinus wave
def generate_sinus(spantime,amplitude,frequency,phase):
    """ Generate a sinus signal 

        Parameters:
            spantime (float or np.array): time point(s) where to evaluate the signal
            amplitude (float or array of size spantime) : amplitude of the signal
            frequency (float or array of size spantime) : frequency of the signal
            phase (float or array of size spantime) : phase of the signal

        Returns:
            signal (np.array): Synthetic sinus signal evaluated at time points
    """

    signal=amplitude*np.sin(2*np.pi*frequency*spantime+phase)
    return signal

### Simple cosinus wave 
def generate_cosinus(spantime,amplitude,frequency,phase):
    """Generate a cosinus signal 
       
       Parameters:
           spantime (float or np.array): time point(s) where to evaluate the signal
           amplitude (float or array of size spantime) : amplitude of the signal
           frequency (float or array of size spantime) : frequency of the signal
           phase (float or array of size spantime) : phase of the signal

        Returns:
           signal (np.array): Synthetic cosinus signal evaluated at time points
       
    """
    signal=amplitude*np.cos(2*np.pi*frequency*spantime+phase)
    return signal

### fourier series 
def generate_fourierseries(spantime,frequency, a,b, n):
    """Generate fourier series
    
        Parameters:
            spantime (float or np.array): time point(s) where to evaluate the signal
            frequency (float or array of size spantime) : frequency of the signal
            a (float or np.array ): sequence of n coefficients a 
            b (float or np.array ): sequence of n coefficients b
            n (int): size of the sequence
        
        Returns :
            signal (np.array): Synthetic fourier series evaluated at time points          
            
    """
    #i,j: int 
    for j in range(0,np.size(frequency)-1):
        signal=a[0][j]
        for i in range(1,n):
            signal=signal+ a[i][j]*np.cos(2*np.pi*frequency(spantime)*spantime*i) + b[i][j]*np.sin(i*2*np.pi*frequency(spantime)*spantime)
    
    #signal=a[0]+np.sum( [ai*np.cos(2*np.pi*frequency*spantime*i) + bi*np.sin(i*2*np.pi*frequency*spantime) for ai,bi in zip(a,b)]
        return signal 

    ### sequences for square shape
def coeffbsquare(n):
    """Generate b sequence for square shape 

    Parameters
    ----------
    n (int): size of sequence

    Returns
    -------
    b (float or np.array): sequence of n coefficients b

    """
    b=np.zeros(n)
    for i in range(0,n):
        if i%2==0:
            b[i]=0
        else:
            b[i]=4/(i*np.pi)
    return b

def coeffasquare(n):
    """ Generate a sequence for square shape
    
        Parameters:
            n (int): size of sequence
        Returns
            a (float or np.array): sequence of n coefficents a
    """
    a=np.zeros(n)
    return a

def coeffsquare(n,amplitude):
    """Generate a and b sequences for square shape
        
       Parameters:
           n (int): size of sequence
           amplitude (float or array of size spantime) : amplitude of the signal
          
       Returns: 
          a (float or np.array): sequence of n coefficents a
          b (float or np.array): sequence of n coefficents b
    """
    a=np.zeros(n)
    b=np.zeros(n)
    for i in range(0,n):
        if i%2==0:
            b[i]=0
        else:
            b[i]=4*amplitude/(i*np.pi)
    
    return a,b
    
    
    ###sequences for triangular shape 

def coefftriangle(n, amplitude):
    """Generate a and b sequences for triangular shape
    
       Parameters:
           n (int): size of sequence
           amplitude (float or array of size spantime) : amplitude of the signal
        
       Returns:
         a (float or np.array): sequence of n coefficents a
         b (float or np.array): sequence of n coefficents b
    """
    a=np.zeros(n)
    b=np.zeros(n)
    for i in range(0,n):
        if i%2==0:
            b[i]=0
        else:
            b[i]=8*amplitude/((i*np.pi)**2)*(-1)**((i-1)/2)
               
    return a,b

### Blood pressure
# todo : docstring
def generate_ABP(spantime,Psystole,Pdiastole,f):
# From linninger 2009

    fouriercos=np.array([-0.0345, -0.0511, -0.0267, -0.0111, -0.0013, 0.0050, 0.0027, 0.0061])
    fouriersin=np.array([0.1009, 0.0284,-0.0160,-0.0070, -0.0174, -0.0041, -0.0041, 0.0005])

    base=sum([fouriercos[i-1]*np.cos(2*i*np.pi*f*spantime) for i in range(1,9)])+sum([fouriersin[i-1]*np.sin(2*i*np.pi*f*spantime) for i in range(1,9)])

    #Current min and max values of the synthetic signal : depends on the fourier coefficients
    minPa=-0.11608417832134309
    maxPa=0.1733161105021536

    #Stretching of the signal to reach the chosen diastolic and systolic blood pressure
    signal=(base-minPa)/(maxPa-minPa)*(Psystole-Pdiastole)+Pdiastole

    return signal

# todo : generate the derivative of generate_ABP function


def linear_process_y_to_x(y_series, lam=0.5, b_x=0.8, sigma2_x=0.2):
    """Linear process: Generate x-series given y-series 

        Parameters:
            x_series (np.array): Duration of the signal
            lam (float) : coupling, domain [0,1] 
            b_y (float): linear coefficient for y-term
            sigma2_y (float): variance noise for y-series 

        Returns:
            y-series (np.array): y-series effected by x-series
    """
    N = y_series.shape[0]
    x_series = np.zeros(N)
    x_series[0] = np.random.normal(0,np.sqrt(sigma2_x),1)
    # loop over indecies in y_serie and calculate x_serie
    for i in range(N - 1):
        eps_x = np.random.normal(0,np.sqrt(sigma2_x),1)
        x_series[i+1] = b_x*x_series[i] + lam*y_series[i] + eps_x
    return x_series


def linear_process_y_to_x_lag_2(y_series, lam=0.5, b_x=0.1, b_x2=0.2, sigma2_x=0.2):
    """Linear process: Generate x-series given y-series 

        Parameters:
            x_series (np.array): Duration of the signal
            lam (float) : coupling, domain [0,1] 
            b_y (float): linear coefficient for y-term
            sigma2_y (float): variance noise for y-series 

        Returns:
            y-series (np.array): y-series effected by x-series
    """
    N = y_series.shape[0]
    x_series = np.zeros(N)
    x_series[0] = np.random.normal(0,np.sqrt(sigma2_x),1)
    x_series[1] = np.random.normal(0,np.sqrt(sigma2_x),1)
    # loop over indecies in y_serie and calculate x_serie
    for i in range(N - 2):
        eps_x = np.random.normal(0,np.sqrt(sigma2_x),1)
        x_series[i+2] = b_x2*x_series[i+1] + b_x*x_series[i] + lam*y_series[i] + eps_x
    return x_series