################################
### Module to perform data analysis
##################################

### Needed libraries
import numpy as np
import matplotlib.pyplot as plt 
import scipy as sp

from scipy import fftpack
import scipy.signal as sg
import datetime

#NFFT
def best_NFFT(duration,samplingrate):
    """Generate the power of 2 closest to the sample number
    
    Parameters
    ----------
    duration (float): Duration of the signal (s)
    samplingrate (float) : Sampling frequency (Hz)

    Returns
    -------
    NFFT (int): length of the FFT used

    """
    nb=duration*samplingrate #total sample number
    
    a=2
    while a<nb:
        a=a*2
    if abs(a-nb)<abs(a/2-nb): #we look at the powers of 2 that frame the values and we return the closest
        return a
    else :
        return a/2
  

#select short senquence
def indexseq(time, duration, spantime):
    """returns a vector of indices corresponding to the observation window
    
    Parameters:
        time (float): Start of the sequence
        duration (float): duration of the sequence
        spantime (float): time points where to evaluate the signal
    Returns:
        indices (np.array):vector of indices 
        
    """
    filtre=(spantime>=time) & (spantime<=(time+duration))
    indices=np.where(filtre)[0]
    return indices


#periodogram calculation
def periodogram(s,spantime,window=sg.hanning):
    """Generate periodogram of a signal
    
    Parameters : 
        s (np.array): the signal to study 
        spantime (float): time points where to evaluate the signal
        window (method): window used to calculate the periodogram
        
    Return :
        frq_fft (np.array): frequencies scale
        s_psd (np.array): signal power spectal density 
    """
    #fourier transform calculation  
    s_fft = fftpack.fft(s*window(len(s)))
    #spectral density (PSD)
    duration=spantime[len(spantime)-1]-spantime[0]
    fs=1/(spantime[1]-spantime[0])
    s_psd=np.abs(s_fft)**2/best_NFFT(duration,fs)
    frq_fft=sp.fftpack.fftfreq(len(s_fft))*fs
    i=frq_fft>0 #select only postitive frequencies 
        
    return frq_fft[i],s_psd[i]

#plot periodogram
def plot_periodogram(frq_fft, s_psd):
    """
    Plot the periodogram of the frequencies and powers in parameters
    Parameters
    ----------
    frq_fft : np.array
        spectrum frequencies 
    s_psd : np.array
        signal power spectal density

    Returns
    -------
    None.

    """
    plt.plot(frq_fft,s_psd)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('PSD')


def searchmainfrequency(signal, spantime,fmin,fmax):
    """ Returns the value of the frequency and its index of the peak between the min and max frequencies
        
        Paramters:
            signal (np.array):signal to study 
            spantime (float): time points where to evaluate the signal
            fmin (float): minimum frequency 
            fmax (float): maximum frequency 
        
        Returns:
            peak_freq0 (float): value of peak frequency 
            ipeak_freq0 (int): peak index
    """
    #fs=1/(spantime[1]-spantime[0])
    
    # sample_freq : frequency vector
    # power : corresponding power vector 
    sample_freq,power=periodogram(signal, spantime)
    
    # Find the peak frequency
    pos_mask = np.where((sample_freq > fmin)&(sample_freq <fmax))  # filtre pour les fequences entre min et max
    freqs = sample_freq[pos_mask] # on ne garde que les frequences du filtre
    ipeak_freq0 = pos_mask[0][0]+power[pos_mask].argmax() # indice du pic
    peak_freq0 = freqs[power[pos_mask].argmax()] # frequence du pic

    return peak_freq0, ipeak_freq0


def searchpeak(signal, spantime,fmin,fmax) :
    """ Calls searchmainfrequency.
        This function is kept to make compatible with earlier versions.
    """
    return searchmainfrequency(signal, spantime, fmin, fmax)


#plot peaks founded 
def plot_peaks(power, peak_freq0, ipeak_freq0):
    """ add red star on the peak of power
    
        Parameters:
            power (np.array): signal power
        
        Returns:
            None
            
    """
    plt.plot(peak_freq0, power[ipeak_freq0], 'r*')
    
    
#filter functions 

def lowpassfilter( signal, fs, cutoff):
    """ Generate low pass filter
        
        Parameters:
            signal (np.array): signal to filtered
            fs (float): sampling frequency 
            cutoff : choosen value for cutoff
        
        Return:
            filteredsingal (np.array): filtered signal
    """
    nyq = 0.5 *fs 
    normal_cutoff = cutoff / nyq
    b, a = sg.butter(5, normal_cutoff, btype='low', analog=False)  
    filteredsignal = sg.filtfilt(b, a, signal)
    return filteredsignal 


def highpassfilter( signal, fs, cutoff):
    """ Generate high pass filter
        
        Parameters:
            signal (np.array): signal to filtered
            fs (float): sampling frequency 
            cutoff : choosen value for cutoff
            
        Return:
            filteredsingal (np.array): filtered signal
    """
    nyq = 0.5 *fs 
    normal_cutoff = cutoff / nyq
    b, a = sg.butter(5, normal_cutoff, btype='high', analog=False)  
    filteredsignal = sg.filtfilt(b, a, signal)
    return filteredsignal 


def bandpassfilter( signal, fs, cutoff1, cutoff2):
    """ Generate band pass filter
        
        Parameters:
            signal (np.array): signal to filtered
            fs (float): sampling frequency 
            cutoff1 : choosen value for low cutoff
            cutoff2 : choosen value for high cutoff
            
        Return:
            filteredsingal (np.array): filtered signal
    """
    nyq = 0.5 *fs 
    normal_cutoff1 = cutoff1 / nyq
    normal_cutoff2 = cutoff2 / nyq
    b, a = sg.butter(5, (normal_cutoff1,normal_cutoff2), btype='bandpass', analog=False)  
    filteredsignal = sg.filtfilt(b, a, signal)
    return filteredsignal


#power in given frequency band 

def power_in_frequency_band(freq, power, fmin, fmax):
    """Calculates sum of power values in a given frequency band
    
        Parameters:
            freq (np.array): frequency scale in PSD
            power (np.aarray): power values in PSD
            fmin (float): minimum frequency of the freaquency band
            fmax (float): maximum frequency of the freaquency band
            
        Returns : 
            power_total (float): sum of power in the frequency band
    """
    filtre=np.where((freq>=fmin) & (freq<=fmax))
    power_total=np.sum(power[filtre])
    return power_total

def add_delta(tme, delta):
    """
    

    Parameters
    ----------
    tme : datetime
        start time.
    delta : deltatime
        time to add.

    Returns
    -------
    datetime
        time of start + delta.

    """
    # transform to a full datetime first
    return (datetime.datetime.combine(datetime.date.today(), tme) + 
            delta).time()


def spectrogram(time,signal,time_window, window=sg.hanning ):
    """
    Plot the spectrgram of the signal without the continuous component. And return time, frequency 
    vectors and power matrix.

    Parameters
    ----------
    time : np.array
        time vector corresponding to the signal.
    signal : np.array
        signal.
    time_window : float
        duration of the time window for periodogram calculation.
    window : method, optional
        type of window for the periodogram calculation. The default is sg.hanning.

    Returns
    -------
    time_res : np.array
        vector of resulting times.
    freq : np.array
        vector of resulting frequencies.
    power_res : np.array
        matrix of resulting power.

    """
    ti=time[0]
    power_res=[]
    time_res=[]

    while ti<time[len(time)-1]-time_window:
        indices=indexseq(ti,time_window,time)
        freq,power=periodogram(signal[indices],time[indices],window)
        if np.size(power_res)==0:
            power_res=power
        else:
            power_res=np.c_[power_res,power]
    
        time_res.append(ti/3600)
        ti=ti+3600
    
    fig=plt.figure(figsize=[14,4])
    ax = fig.add_subplot(122)
    cf = ax.contourf(time_res,freq[1:],10*np.log(power_res[:][1:]*1e6),40,cmap='viridis')
    cbar = fig.colorbar(cf, label='Power (dB)')
    cbar.update_ticks()

    ax.set_xlabel(r'$Time (hour)$')
    ax.set_ylabel(r'$Frequency (Hz)$')
    ax.set_aspect('auto')
    plt.title('Color-scaled PSD ')
    
    return time_res,freq[1:],power_res[:][1:]