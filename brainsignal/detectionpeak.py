# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 16:08:33 2021

@author: Hamon
"""
# import 

import numpy as np 
from scipy import signal 
import brainsignal.datanalysis as da
from scipy.stats import zscore

def peakdetection2 (s, width):
    """
    This function return peaks founded in a signal with function signal.find_peaks_cwt() in
    scipy library.

    Parameters
    ----------
    s : np.array
        signal to study.
    width : np.array
        width of peaks.

    Returns
    -------
    np.array
        peaks indices.

    """
    return signal.find_peaks_cwt(vector=s, widths=[width/20])

def peakdetection4 (s, width):
    """
    This function return peaks founded in a signal with function signal.argrelextrema() 
    in scipy library.

    Parameters
    ----------
    s : np.array
        signal to study.
    width : np.array
        width of peaks.

    Returns
    -------
    peaks index.

    """
    return signal.argrelextrema(s, np.greater, order=int(width/2))




