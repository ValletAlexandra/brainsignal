
from re import I
import numpy as np
import matplotlib.pyplot as plt

import glob
import pandas as pd

import os
import sys
path= '..'
sys.path.append(path)

# Tools from brainsignal library
import brainsignal.datanalysis as da
import brainsignal.preprocessing as bp
import brainsignal.detectionpeak as dp


def supernormalizer(spantime, signals, scale_factor=0.8, fs=200, nt=50, return_time_before_last_peak=False):
    """ This method normalizes a signal so that the number of points
        between two adjacent peaks are constant.

        Parameters:
            spantime (np.array): The times for when the points in the signals are sampled.
            signals (np.array or np.matrix): Either one column with the main signal, 
                                    or a matrix where the first column is the main signal
                                    and where addtinal columns are signals that shall be normalized
                                    w.r.t the main signal. 
            scale_factor (float): Factor to scale the estimate of points in a heart beat.
                                    A factor of 0.8, mens that the number of points are reduced by 
                                    a factor of 1-0.8=0.2.
            fs (int or float): Frequency for the signal
            nt (int): Number of points bewteen adjacent peaks
            return_time_before_last_peak (Boolean): If True, this function return the time
                                    of the minimum signal between the two last peaks. 
                                    This can be used for continous nromalization for a signal
                                    through the night. 

        Returns: 
            all_normalized_arrays (np.array): A matrix where the columns are as follows:
                   1) The normalized time: The time of the normalized points. 
                   2) Virtual time: Time vector where each period is one second.
                   3) Main signal: The normalized signal.
                   4->) If the argument signals contains addtional columns to the main signal, 
                        then the normalization of these signals w.r.t the main signal are returned 
                        as column 5 and onwards in the all_normalized_arrays matrix.  
                   
    """

    N = signals.shape[0]

    # Check if there are additional columns to the main signal.
    if len(signals.shape) > 1:
        n_signals = signals.shape[1]
        main_signal = signals[:,0]
    else:
        n_signals = 1
        # Reshape signal to column vector in matrix so it can be indexed
        # the same way as for more than one signal.
        signals = np.array([signals]).reshape((len(signals), 1))
    main_signal = signals[:,0]

    # Search for the frequency.
    peak_signal, _ = da.searchpeak(main_signal, spantime, fmin=0.5, fmax=2)
    # Estimate the number of points in a wavelength.
    peak_guess = 1/peak_signal*fs 
    # Search for the indices of the peaks.
    # The peak_guess is scaled by scale_factor.
    peaks = dp.peakdetection4(main_signal, peak_guess*scale_factor)[0]

    n_peaks = len(peaks) 
    # Initialzie matrix with a number of columns corresponding to 
    # three time columns, the noralized main signal and additoinal normalized signals.
    all_normalized_arrays = np.zeros((nt*(n_peaks - 1), 2+n_signals))
    normalized_time = all_normalized_arrays[:,0] 

    # Loop through all the peaks 
    for i in range(n_peaks - 1):
        old_time = spantime[peaks[i]:peaks[i+1]]
        new_time = np.linspace(old_time[0], old_time[-1], nt)
        # Run subloop for 1 + # addtinal signals.
        for k in range(n_signals):
            old_signal_k = signals[:,k][peaks[i]:peaks[i+1]]
            new_signal_k = bp.resampling(old_time, old_signal_k, new_time, 0.03) # 0.03 gives good results
            all_normalized_arrays[i*nt:(i+1)*nt,2+k] = new_signal_k
        normalized_time[i*nt:(i+1)*nt] = new_time
    
    # Find the time at the minimum between two last peaks
    last_signal = main_signal[peaks[n_peaks - 2]:peaks[n_peaks - 1]]
    last_time = spantime[peaks[n_peaks - 2]:peaks[n_peaks - 1]]
    time_before_last_peak = last_time[np.argmin(last_signal)]

    # Insert data into the all_normalized_arrays-matrix.
    all_normalized_arrays[:,0] = normalized_time
    n_hb = len(normalized_time)/nt
    virtual_time = np.linspace(0, n_hb, len(normalized_time))
    all_normalized_arrays[:,1] = virtual_time
    
    if return_time_before_last_peak:
        return (all_normalized_arrays, time_before_last_peak)
    else: 
        return all_normalized_arrays


if __name__ == '__main__':
    # See the notebook for a demonstration 
    pass