# -*- coding: utf-8 -*-

# system imports
import os
import sys

# data science
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import seaborn as sns

# signal processing
import scipy.signal
from scipy.ndimage import label
from scipy.stats import zscore
from scipy.interpolate import interp1d
from scipy.integrate import trapz
from scipy import signal 

# misc
import warnings

#import from brainsignal library 
import brainsignal.datanalysis as da 
import brainsignal.detectionpeak as dp
from sklearn.linear_model import LinearRegression




#peak detection using cross-correlation between signal and filter 
def detect_peaks(signal,spantime,frequency=1, threshold=0.6, qrs_filter=None):
    '''Peak detection algorithm using cross corrrelation and threshold 
    
        Parameters:
            signal (np.array): signal to study 
            spantime (np.array): time vector
            threshold (float): threshold to separate the desired features from the rest of the signal
            qrs_filter (np.array): filter
            frequency (float): signal frequency 1 Hz by default
        
        Returns:
            signaltmp[peaks_index] (np.array): signal value corresponding to peaks
            spantime[peaks_index] (np.array): time value corresponding to peaks
            peaks_index (np.array): index of peaks
            similarity (np.array): similarity rate
            
    '''
    signaltmp=signal
    if qrs_filter is None:
        # create default qrs filter, which is just a part of the sine function
        t = np.linspace(1.5 * np.pi*frequency, 3.5 * np.pi*frequency, 15)
        qrs_filter = np.sin(t)
    
    # normalize data
    signal = (signal - signal.mean()) / signal.std()

    # calculate cross correlation
    similarity = np.correlate(signal, qrs_filter, mode="same")
    similarity = similarity / np.max(similarity)
    
    #calculate peaks index
    peaks_index=np.where(similarity > threshold)
    # return peaks (values in s) using threshold
    return signaltmp[peaks_index],spantime[peaks_index],peaks_index, similarity


def get_plot_ranges(start=10, end=20, n=5):
    '''
    Make an iterator that divides into n or n+1 ranges. 
    - if end-start is divisible by steps, return n ranges
    - if end-start is not divisible by steps, return n+1 ranges, where the last range is smaller and ends at n
    
    # Example:
    >> list(get_plot_ranges())
    >> [(0.0, 3.0), (3.0, 6.0), (6.0, 9.0)]

    '''
    distance = end - start
    for i in np.arange(start, end, np.floor(distance/n)):
        yield (int(i), int(np.minimum(end, np.floor(distance/n) + i)))
        

def group_peaks(p, threshold=5):
    '''
    The peak detection algorithm finds multiple peaks for each QRS complex. 
    Here we group collections of peaks that are very near (within threshold) and we take the median index 
    '''
    # initialize output
    output = np.empty(0)

    # label groups of sample that belong to the same peak
    peak_groups, num_groups = label(np.diff(p) < threshold)

    # iterate through groups and take the mean as peak index
    for i in np.unique(peak_groups)[1:]:
        peak_group = p[np.where(peak_groups == i)]
        output = np.append(output, np.median(peak_group))
    return output

# Time-domain features 

def SD (rr):
    """ Calculate standard deviation 
    
    Parameters:
        rr (np.array): array of rr-intervals
    
    Returns:
        sd (float): standard deviation 
    """
    rrmean=np.mean(rr)
    x=np.sum([((rri-rrmean)**2 /np.size(rr)) for rri in zip(rr)])
    sd=np.sqrt(x)
    return sd

def RMSSD (rr):
    """ Calcul of RMSSD
    
    Parameters:
        rr (np.array): array of rr-intervals
    
    Returns:
        rmssd (float): RMSSD
        
    """
    x=0
    for i in range (np.size(rr)-1):
        x=x+(rr[i+1]-rr[i])**2
    rmssd=np.sqrt(x/(len(rr)))
    return rmssd

def NNi(rr,n=0.05):
    """ Number of interval differences of successive RR-intervals greater than n seconds (default 0.05s).
    Parameters:
        rr (np.array): array of rr-intervals
    
    Returns:
        s (float): NNi
    """
    s=np.sum(np.abs(np.diff(rr)) > n)
    return s

def pNNi(rr,n=0.05):
    """The proportion derived by dividing NNi (The number of interval differences \
    of successive RR-intervals greater than n s (default:0.05 s)) by the total number of RR-intervals.
    """
    res=100 * NNi(rr,n) / len(rr)
    return res
    
def CVSD (rr):
    """Coefficient of variation of successive differences equal to the rmssd divided by \
    mean_nni.
    """
    cvsd=RMSSD(rr)/np.mean(rr)
    return cvsd

def range_nni(rr):
    """difference between the maximum and minimum rr_interval.
    """
    return np.max(rr)-np.min(rr)

#sample entropy
def sampen(rr,m=5,r=0.2):
    """ Calculates sample entropy of a time series
    
    Parameters:
        rr (np.array): the input rrnal or series
        m (int): order, the length of template
        r (float): percent of standard deviation
    
    Returns:
        se (float): sample entropy 
    """    
    n = len(rr)
    r = np.std(rr)*r
    
    matchnum = 0.0
    for i in range(n-m):
        tmpl = rr[i:i+m]
        for j in range (i+1,n-m+1):
            ltmp = rr[j:j+m]
            diff = tmpl-ltmp
            if all(diff<r):
                matchnum+=1
    
    allnum = (n-m+1)*(n-m)/2
    if matchnum<0.1:
        se = 1000.0
    else:
        se = -math.log(matchnum/allnum)
    return se  

def timedomain(rr):
    """Generate dictionnary with values of differents features corresponding to rr intervals

    Parameters
    ----------
    rr : np.array 
        RR-intervals

    Returns
    -------
    results : dict
        dictionnary of features in time domain 

    """
    results = {}
    
    results['Mean RR (ms)'] = np.mean(rr)*1e3
    results['STD RR (ms)'] = SD(rr)*1e3
    results['Min RR (ms)'] = np.min(rr)*1e3
    results['Max RR (ms)'] = np.max(rr)*1e3
    results['RMSSD (ms)'] = RMSSD(rr)*1e3
    results['NNxx'] = NNi(rr)
    results['pNNxx (%)'] = pNNi(rr)
    results['CVSD']=CVSD(rr)
    results['range_nni (ms)']=range_nni(rr)*1e3
    results['Sample entropy']=sampen(rr)
    
    return results

#Spectral domain 

def power_frequency_band (frequency, power, fmin, fmax):
    """Calculates power in a given frequency band by integrating
    the sepctral density
    
        Parameters:
            freq (np.array): frequency scale in PSD
            power (np.aarray): power values in PSD
            fmin (float): minimum frequency of the freaquency band
            fmax (float): maximum frequency of the freaquency band
            
        Returns : 
            power_integrated (float): power in the frequency band in ms
    """
    filtre=np.where((frequency>=fmin) & (frequency<=fmax))
    power_integrated=np.trapz(power[filtre], frequency[filtre])
    return power_integrated*1e6

def peak_frequency_band(fxx, pxx, fmin,fmax):
    """find which frequency has the most power in each band

    Parameters
    ----------
    fxx : np.array
        frequency sample.
    pxx : np.array
        power sample.
    fmin : float
        minimum frequency band.
    fmax : TYPE
        maximum frequency band.

    Returns
    -------
    peak_f : float
        frequency which as the most power in the band 

    """
    cond = (fxx >= fmin) & (fxx <=fmax)
    peak_f=fxx[cond][np.argmax(pxx[cond])]
    return peak_f

def LFn(lf, tf, vlf):
    """ Calculates low frequencies ratio 
    
    Parameters:
        lf (float): power in low frequency band
        tf (float): total power of the PSD
        vlf (float): power in very low frequency band
        
    Returns:
        lfn (float): low frequencies ratio
    """
    return lf*100/(tf-vlf)

def HFn (hf, tf, vlf):
    """ Calculates high frequencies ratio 
    
    Parameters:
        hf (float): power in high frequency band
        tf (float): total power of the PSD
        vlf (float): power in very low frequency band
        
    Returns:
        hfn (float): high frequencies ratio
    """
    return hf*100/(tf-vlf)

def frequency_domain(frequency, power):
    """
    Calculate features in frequency domain 

    Parameters
    ----------
    frequency : np.array
        frequency vector.
    power : np.array
        power vector.

    Returns
    -------
    results : dictionary
        dictionary containing features in frequency domain.

    """
    
    results = {}
    results['Power VLF (ms2)'] = power_frequency_band (frequency, power, 0, 0.04)
    results['Power LF (ms2)'] = power_frequency_band (frequency, power, 0.04, 0.15)
    results['Power HF (ms2)'] = power_frequency_band (frequency, power, 0.15, 4)  
    results['Power Total (ms2)'] =results['Power HF (ms2)']+results['Power LF (ms2)']+results['Power VLF (ms2)']

    results['LF/HF'] = results['Power LF (ms2)']/results['Power HF (ms2)']
    results['Peak VLF (Hz)'] = peak_frequency_band(frequency, power, 0,0.04)
    results['Peak LF (Hz)'] = peak_frequency_band(frequency, power, 0.04,0.15)
    results['Peak HF (Hz)'] = peak_frequency_band(frequency, power, 0.15,4)

    results['Fraction LF (%)'] = LFn(results['Power LF (ms2)'],results['Power Total (ms2)'] ,results['Power VLF (ms2)'] )
    results['Fraction HF (%)'] = HFn (results['Power HF (ms2)'], results['Power Total (ms2)'],results['Power VLF (ms2)'] )
    return results


#non-linear features 

def DFA(rr,n):
    """ Calulates Detrended Fluctuation Analysis of rr-intervals 
    
    Parameters:
        rr (np.array):array of rr-intervals
        
    Returns:
        dfa (float): DFA
    """
    N=len(rr)
    t=10 #s considered time scale 
    Nt=int(N/t)
    rrmean=np.mean(rr)
    res=0
    for i in range (1,N):
        for j in range(1,t):
            res=res+1/(t*Nt)*()
            
        res=res+()
    rms=np.sqrt(s/N)
    return rms

    
#performance evaluation 
def Cohens_kappa (po,pe):
    """Calculates Cohen's kappa
    if k<=0 observed agreement is worse than that expected by chance
    if k=1 all the samples were classified into their expected classes
    higher value of k:stronger agreement between the results of designed classifier and the expected results
    
    Parameters:
        po (float): the relative observed agreement among raters
        pe (float):the proportion of agreement expected by chance
    
    Returns:
        k (float): Cohen's kappa
    """
    
    k=(po-pe)/(1-pe)
    return k

def psys(signal,hr, method=dp.peakdetection4):
    """
    Return the mean of systolic pressure 

    Parameters
    ----------
    signal : np.array
        ABP singal to study.
    hr : float
        heart rate.
    method : function, optional
        choose the type of function to detect peaks. The default is dp.peakdetection4.

    Returns
    -------
    float
        mean, std, max, min,median, outliers, n the number of psys in the interval mean +/- std,
        and indexes of systolic pressure.

    """
    fs=200 #Hz
    period_width=1/hr*fs
    pindex=method(signal,period_width)
    psys=signal[pindex]
    outliers=len(psys[np.abs(zscore(psys)) > 2])
    correct=np.where((psys<250) & (psys>0))[0]
    n=(len(psys)-len(np.where((psys<(np.mean(psys)+np.std(psys))) & (psys>(np.mean(psys)-np.std(psys))))[0]))/len(psys)
    psys=psys[correct]

    
    if len(psys)>0:
        return np.mean(psys),np.std(psys),np.max(psys), np.min(psys),np.median(psys),outliers,pindex[0][correct],n
    
    else : 
        return np.nan, np.nan, np.nan, np.nan ,np.nan, np.nan, np.nan, np.nan

def detect_min(time,signal,hr):
    """
    Calculate minimums of a signal 

    Parameters
    ----------
    time : np.array
        time of the signal.
    signal : np.array
        signal to study.
    hr : float
        heart rate.

    Returns
    -------
    pminl : np.array
        list of minimum founded in the signal.
    index_pmin : np.array
        index of mimimums.

    """
    pminl=[]
    index_pmin=[]
    ti=time[0]
    while ti<time[len(signal)-1]:
        index=da.indexseq(ti,1/hr,time)
        pmin=np.min(signal[index])
        #index of min recuperation 
        i=index[0]
        pminl.append(pmin)
        while signal[i]!= np.min(signal[index]):
            i=i+1
        index_pmin.append(i)

        ti=ti+1/hr
    pminl=np.array(pminl)
    index_pmin=np.array(index_pmin)
    correct=np.where(((pminl<150) & (pminl>0)))[0]
    return pminl[correct],index_pmin[correct]
 

def pdia(time,signal, hr):
    """
    Return the mean of diastolic pressure

    Parameters
    ----------
    time : np.array
        time vector
    signal : np.array 
        ABP signal to study.
    hr : flaot 
        heart rate.

    Returns
    -------
    float
        mean, std, max, min, outliers and indexes of diastolic pressure.

    """  
    
    pdia,index_min=detect_min(time,signal,hr)
    if len(pdia)>0:
        p=signal[index_min]
        outliers=len(p[np.abs(zscore(p)) > 2])
        return np.mean(pdia),np.std(pdia),np.max(pdia), np.min(pdia),outliers, index_min
    else : 
        return np.nan,np.nan,np.nan,np.nan,np.nan,np.nan

def amplitude_ABP(time,sign,ipmax,ipmin,hr):
    """
    Calculate the amplitude between a max and a successive min

    Parameters
    ----------
    time : np.array
        time vector of the signal.
    sign : np.array
        signal.
    ipmax : np.array
        indices of maximums.
    ipmin : np.array
        indices of minimums.
    hr : float
        heart rate.

    Returns
    -------
    ampl : np.array
        amplitude vector.
    tampl : np.array
        time vector corresponding to amplitude (min times).
    pdia_res : np.array
        diastolic pressure vector corresponding to  the amplitude
    """
    if (len(ipmin)>0) & (len(ipmax)>0) :
        tampl=[] #amplitude time
        ampl=[] # amplitude array
        t_psys=time[ipmax] # time of psys
        t_pdia=time[ipmin] #time of pdia
        psys=sign[ipmax] #list of psys 
        pdia=sign[ipmin] #list of pdia
        pdia_res=[] #list of diastolic pressure used to calculate the amplitude

        for ti in t_psys :
            index_pdia=np.where((t_pdia>ti-1/hr) & (t_pdia<ti))
            index_psys=np.where(ti==t_psys)
            if(np.size(psys[index_psys]-pdia[index_pdia])>0):
                ampl.append(psys[index_psys][0]-pdia[index_pdia][0])
                tampl.append(t_pdia[index_pdia][0])
                pdia_res.append(pdia[index_pdia][0])
        
        return np.array(ampl, dtype=float),np.array(tampl,dtype=float),np.array(pdia_res,dtype=float)
    else : 
        return np.nan,np.nan,np.nan

def rr_intervals (s,time,method):
    """
    This function returns rr_intervals values during time of the sample

    Parameters
    ----------
    s : np.array
        signal sample.
    time : np.array
        time of the sample.
    method : varying
        it is the method selected for peaks detection.

    Returns
    -------
    time_rr : np.array
        time corresponding to rr_intervals values.
    rr_corrected : np.array
        rr_intervals values.
    hr : float 
        heart rate
    power_hr : float 
        power of heart frequency peak
    n : float
        number of corrected rr-intervals
    power_integrated : float
        integral value under the peak of heart frequency

    """
    
    fs=200 #Hz sampling frequency 
    #calculation of period_width
    #periodogram
    frequency,power=signal.periodogram(s,fs,window='hanning',nfft=None ,scaling='density')
    #Heart rate 
    hr,index_hr=da.searchpeak(s,time,fmin=0.65,fmax=3)
    power_hr=power[index_hr+1]*1e6
    power_integrated=power_frequency_band(frequency, power, 0.65, 3)
    period_width=1/hr*fs
    #peak detection 
    peak_index=method(s,period_width)
    # time conrresponding to peaks
    time_peaks=time[peak_index]
    # RR-intervals are the differences of time between successive peaks
    rr = np.diff(time_peaks)
    time_rr=time_peaks[0:-1] 
    rr_corrected = rr.copy()
    rr_corrected[np.abs(zscore(rr)) > 2] = np.median(rr)
    n=len(rr_corrected[np.abs(zscore(rr)) > 2])


    return time_rr,rr_corrected,hr, power_hr,n,power_integrated