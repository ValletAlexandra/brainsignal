# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 2021

@author: Hamon
"""


# system imports
import os
import sys

# data science
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import seaborn as sns

# signal processing
import scipy.signal
from scipy.ndimage import label
from scipy.stats import zscore
from scipy.interpolate import interp1d
from scipy.integrate import trapz
from scipy import stats

# misc
import warnings

#import from brainsignal library 
import brainsignal.datageneration as dg
import brainsignal.datanalysis as da 
import brainsignal.detectionpeak as dp
from sklearn.linear_model import LinearRegression
from scipy.interpolate import InterpolatedUnivariateSpline


def ICP_max(signal,hr, method=dp.peakdetection4):
    """
    Return the mean of max values in ICP signal. 

    Parameters
    ----------
    signal : np.array
        ICP singal to study.
    hr : float
        heart rate.
    method : function, optional
        choose the type of function to detect peaks. The default is dp.peakdetection4.

    Returns
    -------
    icp_max : np.array
        values of ICP max.
    pindex : np.array
        index of maximum founded

    """
    
    fs=200 #Hz
    period_width=1/hr*fs
    pindex=method(signal,period_width)
    icp_max=signal[pindex]
    
    return icp_max,pindex

def detect_min(time,signal,hr):
    """
    Calculate minimums of a signal 

    Parameters
    ----------
    time : np.array
        time of the signal.
    signal : np.array
        signal to study.
    hr : float
        heart rate.

    Returns
    -------
    pminl : np.array
        list of minimum founded in the signal.
    index_pmin : np.array
        index of mimimums.

    """
    pminl=[]
    index_pmin=[]
    ti=time[0]
    while ti<time[len(signal)-1]:
        index=da.indexseq(ti,1/hr,time)
        pmin=np.min(signal[index])
        pminl.append(pmin)
        #index of min recuperation 
        i=index[0]
        while signal[i]!= np.min(signal[index]):
            i=i+1
        index_pmin.append(i)
        ti=ti+1/hr
    
    return pminl,index_pmin

def ICP_min(time,signal, hr):
    """
    Return the mean of min values in ICP signal

    Parameters
    ----------
    signal : np.array 
        ICP signal to study.
    hr : float 
        heart rate.

    Returns
    -------
    float
        mean of ICP min.

    """   
    icp_min,index_min=detect_min(time,signal,hr)
    
    return np.mean(icp_min)    

def amplitude_ICP(time,sign,ipmax,ipmin,hr):
    """
    Calculate the amplitude between a max and a successive min

    Parameters
    ----------
    time : np.array
        time vector of the signal.
    sign : np.array
        signal.
    ipmax : np.array
        indices of maximums.
    ipmin : np.array
        indices of minimums.
    hr : float
        heart rate.

    Returns
    -------
    ampl : np.array
        amplitude vector.
    tampl : np.array
        time vector corresponding to amplitude (min times).
    pdia_res : np.array
        diastolic pressure vector corresponding to  the amplitude
    """
    tampl=[] #amplitude time
    ampl=[] # amplitude array
    t_psys=time[ipmax] # time of psys
    t_pdia=time[ipmin] #time of pdia
    psys=sign[ipmax] #list of psys 
    pdia=sign[ipmin] #list of pdia
    pdia_res=[] #list of diastolic pressure used to calculate the amplitude

    for ti in t_psys :
        index_pdia=np.where((t_pdia>ti-1/hr) & (t_pdia<ti))
        index_psys=np.where(ti==t_psys)
        if(np.size(psys[index_psys]-pdia[index_pdia])>0):
            ampl.append(psys[index_psys][0]-pdia[index_pdia][0])
            tampl.append(t_pdia[index_pdia][0])
            pdia_res.append(pdia[index_pdia][0])
        
    return np.array(ampl, dtype=float),np.array(tampl,dtype=float),np.array(pdia_res,dtype=float)


def respiratory_frequency(time, amplitude):
    """
    Return the mean of respiratory frequencies founded in 1 minute size window.

    Parameters
    ----------
    time : np.array
        time vector.
    amplitude : np.array
        amplitude vector.

    Returns
    -------
    float
        mean  & standard deviation of respiratory frequency.
    

    """
    resp_list=[]
    ti=time[0]

    while ti<time[len(time)-1]-60 :
        indices=da.indexseq(ti, 60, time)
    
        # time vector creation 
        spantime=dg.generate_time(60,10,ti)

        # interpolation 
        idx=np.where(np.diff(time[indices])>0.0)[0]
        ampl_interpolated=InterpolatedUnivariateSpline(time[indices][idx],amplitude[indices][idx])
        #respiratory frequency 
        resp,index_resp=da.searchpeak(ampl_interpolated(spantime),spantime,fmin=0.16,fmax=0.8)
        resp_list.append(resp)
        ti=ti+60
    return np.mean(resp_list),np.std(resp_list)

def slope (pdia, ampl, time_ampli):
    """
    Calculate mean slope and parameters in a defined time window.

    Parameters
    ----------
    pdia : np.array
        Diastolic pressure.
    ampl : np.array
        Amplitude.
    time_ampli : np.array
        time corresponding to amplitudes calculated.

    Returns
    -------
    float
        mean and standard deviation of slope and parameters (intercept,rvalue, pvalue, stderr).

    """
    
    time_slide=60 #seconds
    slope_list=[]
    intercept_list=[]
    rvalue_list=[]
    pvalue_list=[]
    stderr_list=[]   
    ti=time_ampli[0]

    while ti<time_ampli[len(time_ampli)-1]-time_slide :
        indices=da.indexseq(ti,time_slide, time_ampli)
    
        # slope calculation 
        slope,intercept,rvalue,pvalue,stderr= stats.linregress(pdia[indices], ampl[indices])
        slope_list.append(slope)
        intercept_list.append(intercept)
        rvalue_list.append(rvalue)
        pvalue_list.append(pvalue)
        stderr_list.append(stderr)
        ti=ti+time_slide
    
    return np.mean(slope_list),np.std(slope_list),np.mean(intercept_list),np.std(intercept_list), np.mean(rvalue_list), np.std(rvalue_list),np.mean(pvalue_list),np.std(pvalue_list),np.mean(stderr_list),np.std(stderr_list) 
