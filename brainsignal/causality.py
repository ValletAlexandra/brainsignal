import numpy as np
import matplotlib.pyplot as plt
#from torch import frac
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.stattools import grangercausalitytests
from statsmodels.tsa.api import VAR
from statsmodels.tools.sm_exceptions import (
    InfeasibleTestError
)
import brainsignal.datanalysis as da
import brainsignal.preprocessing as pp


def stationariy_p_value(series):
    """ Test for non-stationarity by calling the adfuller method
        in the statsmodel library.
        The null-hypothesis is that the series is not stationary.
        A low p-value is an indication of stationarity.

        Parameters:
            series (np.array): Series to be tested for stationarity.
        
        Returns:
            p_value (float): p-value for series being non-stationary.
    """
    result = adfuller(series)
    p_value = result[1]
    return p_value


def aic_lags(x, y, maxlag=15):
    """ For each lag, calculate the AIC.
        The lag is the number of previous terms in the y-series
        that are used to predict the current value of the x-series.
        A lag of 3 means we have the model
        Y_t = const + A_1 Y_{t-1} + A_2 Y_{t-2} + A_3 Y_{t-3} + \epsilon, 
        where \epsilon \sim N(0, \Sigma).

        Parameters:
            x (np.array): The dependent time-series.
            y (np.array): The independent time-series.
            maxlag (int): How many lags that shall be calculated.

        Returns:
            aic_arr (np.array): Array of AIC values from the lags.
    """
    data_mat = np.zeros((len(x), 2))
    data_mat[:,0] = x
    data_mat[:,1] = y

    aic_arr = np.zeros(maxlag)
    model = VAR(data_mat)
    for i in range(maxlag):
        result = model.fit(i+1)
        aic_arr[i] = result.aic
    return aic_arr


def gc_values(x, y, maxlag=15, return_dict=False):
    """ Uses the grangercausalitytests from statsmodels to calculate 
        the Granger causality from y to x. Note the order, we test if 
        the second argument Granger causes the frist argument.

        Parameters:
            x (np.array): Signal to check if is Granger caused by the second signal.
            y (np.array): Signal to check if Granger causes the first signal.
            maxlag (int): Maximum number of lags behind to check for Granger ausality.
            return_dict (Boolean): Boolean to decide if the dictionry from the 
                            grangercausalitytests-call should be return as the forth element.

        Returns:
            best_lag (int): The best lag. 
            F_stat (float): The F-statisic for whether or not y Granger causes x
            p_value (float): The p-value for whether or not y Granger causes x
            gc_dict (dict): The dictionary with values from all lags. 
                            Is return only if the argument return_dict is True.
    """

    data_mat = np.zeros((len(x), 2))
    data_mat[:,0] = x
    data_mat[:,1] = y

    lag_arr = aic_lags(x,y, maxlag=maxlag)
    best_lag = np.argmax(lag_arr) + 1

    # Return nan's if the gc-test return error due to 0's 
    try:
        gc_dict = grangercausalitytests(data_mat, maxlag=best_lag, verbose=False)
    except InfeasibleTestError:
        return [np.nan]*3

    F_stat, p_value = gc_dict[best_lag][0]['ssr_ftest'][:2]

    # Only return the whole dictionary if return_dict is set to True.
    if return_dict:
        return best_lag, F_stat, p_value, gc_dict
    else:
        return best_lag, F_stat, p_value


def causality_statistics(spantime, ICP, ABP, fs=200, max_missing_values=0.2, standardize=False, maxlag=15, resample=False, resampling_frequency=200):
    """
    causality_statistics takes in two signals and calculates the causality index
    Granger causality. It uses the grangercausalitytests from the statsmodel library.
    Both the casality for how ABP affect ICP, and how ICP affects ABP are calcuates. 
    The tests results are an F-statistic and a p-value for the best lag. 
    min, max, median and statistd divations are calcuated for the best lag.
    All results are reutrn as a dictionary.

        Parameters:
            spantime (np.array): Array with the time.
            ICP (np.array): The ICP signal
            ABP (np.array): The ABP signal
            fs (int): The requency of the signal.
            max_missing_values (float): The maxiumum fraction of nan-values.
            standardize (bool): If the signal shall be standardized or not.
            maxlag (int): The maximun number of lags to check.
            resample (bool): If the signal shall be resampled.
            resampling_frequency (int): The resampled frequency.

        Returns:
            result_dict (dict): A dictionary of results on the following form:

                {
                    "y_to_x":{
                        "lag":{
                            "min":
                            "max":,
                            "median":,
                            "std":
                        },
                        "F":{
                            "min":,
                            "max":,
                            "median":,
                            "std":
                        },
                        "p":{
                            "min":,
                            "max":,
                            "median":,
                            "std":
                        },
                        "percentage_nans":
                    },
                }
            It also contains the same results for "x_to_y".
    """

    # resample signal
    if resample:
        new_spantime = np.linspace(spantime[0], spantime[-1], round((spantime[-1] - spantime[0])*resampling_frequency)+1)
        ICP = pp.resampling(spantime, ICP, new_spantime, window_length_time=0.1) 
        ABP = pp.resampling(spantime, ABP, new_spantime, window_length_time=0.1)
        spantime = new_spantime

    # standardize signal
    if standardize:
        ICP = pp.standardize(ICP)
        ABP = pp.standardize(ABP)

    hr, _ = da.searchmainfrequency(ABP, spantime,fmin=0.65,fmax=3)
    points_per_hr_beat = np.round(fs*hr).astype('int')
    N = len(spantime)
    beats_per_window = np.floor(N/points_per_hr_beat).astype('int')
    stats_matrix_y_to_x = np.zeros((beats_per_window, 3))
    stats_matrix_x_to_y = np.zeros((beats_per_window, 3))
    for i in range(beats_per_window):
        indices = np.arange(i*points_per_hr_beat,(i+1)*points_per_hr_beat)
        stats_matrix_y_to_x[i,:] = gc_values(ICP[indices], ABP[indices], maxlag=maxlag)
        stats_matrix_x_to_y[i,:] = gc_values(ABP[indices], ICP[indices], maxlag=maxlag)
    result_dict = {}
    direction_names = ['y_to_x', 'x_to_y']
    direction_matrices = [stats_matrix_y_to_x, stats_matrix_x_to_y]
    tests = ['lag', 'F', 'p']
    statistics_names = ['min', 'max', 'median', 'std']
    #statistics_funcs = [np.min, np.max, np.median, np.std]
    statistics_funcs = [np.nanmin, np.nanmax, np.nanmedian, np.nanstd]
    for i in range(len(direction_matrices)):
        result_dict[direction_names[i]] = {}
        for j in range(len(tests)):
            result_dict[direction_names[i]][tests[j]] = {}
            for k in range(len(statistics_names)):
                result_dict[direction_names[i]][tests[j]][statistics_names[k]] = {}
                number_of_nan_values = np.sum(np.isnan(direction_matrices[i]), axis=0)
                # Write nan if number of nan's is larger than trehshold
                if np.max(number_of_nan_values) >= max_missing_values:
                    result_dict[direction_names[i]][tests[j]][statistics_names[k]] =\
                       np.nan 
                else:
                    result_dict[direction_names[i]][tests[j]][statistics_names[k]] =\
                        statistics_funcs[k](direction_matrices[i], axis=0)[j]
        # Warning: will devide by zero if beats_per_window is 0.
        result_dict[direction_names[i]]['percentage_nans'] = np.max(number_of_nan_values)/beats_per_window
    return result_dict


if __name__ == "__main__":
    pass