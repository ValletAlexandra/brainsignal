#!/bin/sh

#!/bin/sh

# First, the patient data must be in a folder called 'data' at the same level as the scripts-folder.
# The script runs causalityfeatures.py on all files in directors staritn with "PatID" 
# To run on mac/linux, type <chmod +x run_causality.sh> the first time, and then call the script by <./run_causality.sh>


# Remember to add the optinal arugment them in the call below, just like --patient_dir is.
# To get an overview and description of the optinal aguments, run <python causalityfeatures.py -h>

# Optional arguments are:
#data_dir
#save_dir
#patient_dir
#study_name
#time_window
#time_slide
#initial_time
#standardize
#resample 
#resampling_frequency

for f in ../data/PatID* ; do
    patient_dir=$(basename $f)
    python causalityfeatures.py --patient_dir "$patient_dir" -rs True -rf 20
done