
# -*- coding: utf-8 -*-
#import
import os
import sys
import re

path = os.path.abspath(os.path.join('..'))
sys.path.append(path)
 
import brainsignal.datageneration as dg
import brainsignal.datanalysis as da
import brainsignal.HRVanalysis as ha
import brainsignal.detectionpeak as dp
import brainsignal.ICPanalysis as ia
import brainsignal.causality as ca
import brainsignal.supernormalizer as sn
import brainsignal.preprocessing as pp
import numpy as np
import scipy as sp
import glob
import pandas as pd
import seaborn as sns
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.stats import zscore
from scipy import signal
import time 
import datetime
import argparse

import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Calculate causality data for one patient.')
parser.add_argument('-dd', '--data_dir', default='../data/', help='Directory to the data')
parser.add_argument('-sd','--statistics_dir', default='saved_supernormalized/', help='Folder to save statistics data')
parser.add_argument('-pd', '--patient_dir', default='PatID 2_Txt Files', help='Folder to patientdata')
parser.add_argument('-sn', '--study_name', default='', help='Special name for study')
parser.add_argument('-tw', '--time_window', default=60, type=int, help='Time window')
parser.add_argument('-ts', '--time_slide', default=60, type=int, help='Time slide')
parser.add_argument('-it', '--initial_time', default=0, type=int, help='Initial time')
parser.add_argument('-num', '--number_of_points', default=50, type=int, help='Number of per wave length')
args = parser.parse_args()
if '\n' in args.patient_dir: args.patient_dir = args.patient_dir.replace('\n', ' ')


# save names and create folders
data_dir = args.data_dir
statistics_dir = args.statistics_dir
patient_dir = args.patient_dir 
study_name = '_' + args.study_name if args.study_name else args.study_name # Add underscore before study name.
patientid = int(re.findall(r'\d+', patient_dir)[0]) # use regex to extract the PatID
time_window = args.time_window
time_slide = args.time_slide
initial_time = args.initial_time
n_points = args.number_of_points
study_time = datetime.date.today().strftime('%Y%m%d_') + time.strftime('%H%M%S')
save_name = f'Pat_{patientid}_{study_time}_window_{time_window}_slide_{time_slide}{study_name}'

output_folder = data_dir + statistics_dir + save_name
if not os.path.exists(output_folder):
    os.makedirs(output_folder)
    
# Create output files
output_file=open(output_folder + '/' + save_name + '_supernorm_data.txt', 'w')
output_log=open(output_folder + '/' + save_name + '_supernorm_log.txt','w')

# ABP data
file_ABP=glob.glob(data_dir + patient_dir + '/WAVE_ABP_*', recursive=True)[0]
data_ABP=pd.read_csv(file_ABP,header=None, decimal=',', delimiter=';')
data_array_ABP=np.array(data_ABP.values)
ABP_signal=data_array_ABP.transpose()[0]

# ICP data
file_IPC = glob.glob(data_dir + patient_dir +'/WAVE_ICP*', recursive=True)[0]
data_IPC=pd.read_csv(file_IPC,header=None, decimal=',', delimiter=';')
data_array_IPC=np.array(data_IPC.values)
ICP_signal=data_array_IPC.transpose()[0]

if len(ABP_signal) != len(ICP_signal):
    exit("Signals do not have same length")
signal_matrix = np.zeros((len(ABP_signal), 2))
signal_matrix[:,0] = ABP_signal
signal_matrix[:,1] = ICP_signal

fs=200 #constant

spantime=np.arange(len(ABP_signal))/fs
time_tot=len(spantime)/fs # total time of aquisition

t=time.process_time()
first=True
ti = initial_time

df = pd.read_excel('../data/times.xls')
patient_str = 'ID' + str(patientid)
line=np.where(df['Patient']==patient_str)[0][0]
time_norm =df['Initial time'][line]
t_norm=(time_norm.hour*3600+time_norm.minute*60+time_norm.second+ti+time_window/2)-23*3600

counter = 0
print('Start of patient: ', patientid)

past_virtual_time = 0
first_interval = True

while ti<time_tot-time_window:
    
    if first_interval:
        indices = da.indexseq(ti, time_window, spantime)
        first_interval = False
    else:
        indices = da.indexseq(time_before_last_peak, (ti-time_before_last_peak) + time_slide, spantime)

    data_matrix, time_before_last_peak = sn.supernormalizer(spantime[indices], signal_matrix[indices], nt=n_points, return_time_before_last_peak=True)

    supernormalized_time = data_matrix[:,0]
    virtual_time = data_matrix[:,1]
    super_ABP = data_matrix[:,2]
    super_ICP = data_matrix[:,3]

    # Add previous time to the virtual for continuity
    virtual_time += past_virtual_time
    past_virtual_time = virtual_time[-1]

    time_avg=ti+time_window/2

    # print to the output file
    # if first time called, write the item names
    if first :
        output_file.write("#time\t" ) 
        output_file.write("#time_normalized\t" ) 
        output_file.write("supernormalized_time\t")
        output_file.write("virtual_time\t")
        output_file.write("supernormalized_ABP\t")
        output_file.write("supernormalized_ICP\t")
        output_file.write("\n")
                
        first=False
            
    for i in range(len(super_ABP)-1):
        output_file.write(f"{time_avg}\t")
        output_file.write(f"{t_norm}\t")
        output_file.write(f"{supernormalized_time[i]}\t")
        output_file.write(f"{virtual_time[i]}\t")
        output_file.write(f"{super_ABP[i]}\t")
        output_file.write(f"{super_ICP[i]}\t")
        output_file.write("\n")

    #incrementation to go to next time window
    ti=ti+time_slide
    t_norm=t_norm+time_slide
        
    print(patientid, counter, ti)
    counter += 1

#fill the analysis informations file
time_execution = time.process_time() -t  #calculate total  time of execution
output_log.write("date and time of the analysis : %s \n" % datetime.datetime.now().strftime("%H:%M:%S %d/%m/%Y ") )  
output_log.write("Choice of the window size: %s seconds\n" % time_window)
output_log.write("Total time of execution: %s seconds \n" % time_execution )

output_file.close()
output_log.close()
    
print('End of patient: ', patientid)
#if __name__ == "__main__":
#    pass
