# -*- coding: utf-8 -*-
"""
Created on Tue May 18 10:47:12 2021

@author: Hamon
"""

import brainsignal.datageneration as dg
import matplotlib.pyplot as plt
import numpy as np

spantime=dg.generate_time(120, 10)

a,b=dg.coeffsquare(1000,1)

signal=dg.generate_fourierseries(spantime,0.05,a,b,1000)
courbe=plt.plot(spantime,signal)
