# -*- coding: utf-8 -*-
"""
Created on Wed Jul  7 17:24:28 2021

@author: Hamon
"""

#import
 
import os
import sys
path=r'C:\Users\Hamon\Documents\Stage\brainsignal'
sys.path.append(path)
import brainsignal.datageneration as dg
import brainsignal.datanalysis as da
import brainsignal.HRVanalysis as ha
import brainsignal.detectionpeak as dp
import brainsignal.ICPanalysis as ia
import numpy as np
import scipy as sp
import glob
import pandas as pd
import seaborn as sns
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.stats import zscore
from scipy import stats
from scipy import signal
import time 
import datetime

#load data 

rep='../../data/'
patientdir='ID_3' #dossier du patient 
study_name='window5min'


#ICP data 

file=glob.glob(rep+patientdir+'/WAVE_ICP.txt',recursive=True)[0]

data=pd.read_csv(file,header=None, decimal=',', delimiter='\n')
data_array=np.array(data.values)
ICP_signal=data_array.transpose()[0]


# parameters
time_window=300 #s durée d'extraction du signal pour le calcul des features
time_slide=60 #s glissement de la fenêtre 
freq_resampling=3.41 #Hz
method=dp.peakdetection4

fs=200 #constant

ICP_time=np.arange(len(ICP_signal))/fs
time_tot=len(ICP_time)/fs # total time of aquisition

## creation of a file to write the output

outputfolder='../../output/'
if not os.path.exists(outputfolder):
    os.makedirs(outputfolder)
# Create output files
# it will be erased and rewriten at each call of the script
output_file=open(outputfolder+patientdir+study_name+'ICP.txt', 'w')
output_log=open(outputfolder+patientdir+study_name+'ICP_log.txt','w')

#initialisation avec une liste vide 
failed_times=[]
nb_analysed_windows=0
    

ti=30 #int
t=time.process_time()
first=True
date = datetime.datetime.now()

# calculation of normalized time
df = pd.read_excel ('C:/Users/Hamon/Documents/Stage/data/times.xls')
line=np.where(df['Patient']==patientdir)[0][0]
time_norm =df['Initial time'][line]
t_norm=(time_norm.hour*3600+time_norm.minute*60+time_norm.second+ti+time_window/2)-23*3600

while ti<time_tot-time_window:
    
    #we count the number of analysed time windows
    nb_analysed_windows+=1
    
    indices=da.indexseq(ti,time_window, ICP_time)
    time_avg=ti+time_window/2
    
    # ICP features
    # periodogram 
    frequency_icp,power_icp=signal.periodogram(ICP_signal[indices],fs,window='hanning',nfft=None ,scaling='density')
    
    #Heart rate 
    hr_icp,index_hr=da.searchpeak(ICP_signal[indices],ICP_time[indices],fmin=0.8,fmax=2)
    power_hr=power_icp[index_hr+1]*1e6
    #mean ICP
    mean_ICP=np.mean(ICP_signal[indices])
    #systolic & diastolic pressure determination
    icp_max,psysindex=ia.ICP_max(ICP_signal[indices],hr_icp)
    icp_min,pdiaindex=ia.detect_min(ICP_time[indices],ICP_signal[indices],hr_icp)
    
    #amplitude calculation
    amplitude,tamp,pdia=ia.amplitude_ICP(ICP_time[indices], ICP_signal[indices], psysindex, pdiaindex, hr_icp)
    
    #amplitude correction 
    ampl_corrected=amplitude.copy()
    ampl_corrected[np.abs(zscore(amplitude)) > 2] = np.median(amplitude)
    # this correction method may be ineffective we could try later a method using the derivative
    #slope 
    slope,slope_std,intercept,intercept_std, rvalue,rvalue_std, pvalue,pvalue_std,stderr,sterrstd = ia.slope(pdia, ampl_corrected,tamp)
    
    # relative mean error :
    ypred=slope*pdia+intercept
    ydata=ampl_corrected
    err=1/len(pdia)*sum(abs(ypred-ydata)/ydata)*100
    
    #respiratory frequency 
    respi,stdrespi=ia.respiratory_frequency(tamp, ampl_corrected)

        #print to the output file
    
        # if first time called, write the item names
    if first :  
        output_file.write("#time\t" ) 
        output_file.write("Heart frequency ICP (Hz)\t") 
        output_file.write("Power hr ICP (ms2)\t")
        output_file.write("Respiratory frequency mean (Hz)\t") 
        output_file.write("Respiratory frequency std (Hz)\t") 
        output_file.write("Mean ICP (mmHg)\t" ) 
        output_file.write("STD ICP (mmHg)\t" )
        output_file.write("Mean AMP (mmHg)\t" )
        output_file.write("STD AMP (mmHg)\t" )
        output_file.write("Psys ICP mean (mmHg)\t")
        output_file.write("Psys STD ICP (mmHg)\t" )
        output_file.write("Pdia ICP mean (mmHg)\t")
        output_file.write("Pdia STD ICP  (mmHg)\t" )
        output_file.write("Slope\t")
        output_file.write("Error slope\t")
        output_file.write("Intercept\t")
        output_file.write("rvalue\t")
        output_file.write("pvalue\t")
        output_file.write("std err\t")   
        output_file.write("time normalized\t" )
        
        first=False
            
        #line change
        output_file.write("\n")

    output_file.write("%f\t" % time_avg)
    output_file.write("%f\t" % hr_icp) 
    output_file.write("%.3e\t" % power_hr)
    output_file.write("%f\t" % respi)
    output_file.write("%f\t" % stdrespi)          
    output_file.write("%.3e\t" % mean_ICP)
    output_file.write("%.3e\t" % np.std(ICP_signal[indices]))
    output_file.write("%.3e\t" % np.mean(ampl_corrected))
    output_file.write("%.3e\t" % np.std(ampl_corrected))
    output_file.write("%.3e\t" % np.mean(icp_max))
    output_file.write("%.3e\t" % np.std(icp_max))
    output_file.write("%.3e\t" % np.mean(icp_min))
    output_file.write("%.3e\t" % np.std(icp_min))
    output_file.write("%.3e\t" % slope)
    output_file.write("%.3e\t" % err)
    output_file.write("%.3e\t" % intercept)
    output_file.write("%.3e\t" % rvalue)
    output_file.write("%.3e\t" % pvalue)
    output_file.write("%.3e\t" % stderr)
    output_file.write("%f\t" % t_norm)
    
    #line change
    output_file.write("\n")

            
    #incrementation to go to next time window
    ti=ti+time_slide
    t_norm=t_norm+time_slide

#fill the analysis informations file
time_execution = time.process_time() -t  #calculate total  time of execution

output_log.write("date and time of the analysis : %s \n" % date.strftime("%H:%M:%S %d/%m/%Y ") )  
output_log.write("Choice of the window size: %s seconds\n" % time_window)
output_log.write("Choice of the peak detection method: %s \n" % method)
output_log.write("Total number of analysed windows: %s \n" % nb_analysed_windows)
output_log.write("Total time of execution: %s seconds \n" % time_execution )
output_log.write("Problematic times: %s \n" % failed_times)
output_log.write("Number of failed times: %s \n" % len(failed_times))



# calculation periodogram of mean ICP all night long on hour windows
##file reading to get mean ICP values 

###data loading and reading
file=glob.glob(outputfolder+patientdir+study_name+'ICP.txt',recursive=True)[0]

data=pd.read_csv(file, decimal='.', delimiter='\t')
variables=data.columns
time=np.array(data['#time'])
mean_icp=np.array(data['Mean ICP (mmHg)'])

#reopen the file to write results
output_file=open(outputfolder+patientdir+study_name+'ICP.txt','w')

# parameters
size_window=3600 #s
ti=30 #seconds
first=True
i=0
while ti < time[len(time)-1]-size_window:
    
    #time selection 
    indices=da.indexseq(ti, size_window, time)
    #periodogram calculation 
    freq,power=da.periodogram(mean_icp[indices], time[indices])
    # power integral calculation 
    p_integrated=ha.power_frequency_band(freq[1:],power[1:],fmin=0.0001,fmax=0.008)
    
    ## write in the file
    ### write name of variables
    if first : 
        for var in variables[:-1] :
            output_file.write("%s\t" % var ) 
        
        output_file.write("Power ICP integrated\t" ) 
        output_file.write("\n" ) 
        first=False
    
    ### write values
    for var in variables[:-1]:
        
        output_file.write("%f\t" % data[var][i])
    output_file.write("%f\t" % p_integrated)
    #line change       
    output_file.write("\n")
    

    # go to next time window
    i=i+1
    ti=ti+time_slide

#for the last hour we can't calculate periodograms so we fill remaining values for the other variables  
while i<len(data):
    for var in variables[:-1]:
        output_file.write("%f\t" % data[var][i])      
    output_file.write("\n")
    i=i+1

output_file.close()
output_log.close()