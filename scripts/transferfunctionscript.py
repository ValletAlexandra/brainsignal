# -*- coding: utf-8 -*-
"""
Created on Fri Jul  9 2021

@author: Hamon
"""
#import
 
import os
import sys
path=r'C:\Users\Hamon\Documents\Stage\brainsignal'
sys.path.append(path)
import brainsignal.datageneration as dg
import brainsignal.datanalysis as da
import brainsignal.HRVanalysis as ha
import brainsignal.detectionpeak as dp
import brainsignal.ICPanalysis as ia
import brainsignal.transferfunction as tf
import numpy as np
import scipy as sp
import glob
import pandas as pd
import seaborn as sns
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.stats import zscore
from scipy import stats
from scipy import signal
import time 
import datetime

#load data 
rep='../../data/'
patientdir='ID_5' #dossier du patient 
study_name='window5min'

#ABP data
file=glob.glob(rep+patientdir+'/WAVE_ABP.txt', recursive=True)[0]

data=pd.read_csv(file,header=None, decimal=',', delimiter='\n')
data_array=np.array(data.values)
ABP_signal=data_array.transpose()[0]

#ICP data
file=glob.glob(rep+patientdir+'/WAVE_ICP.txt', recursive=True)[0]

data=pd.read_csv(file,header=None, decimal=',', delimiter='\n')
data_array=np.array(data.values)
ICP_signal=data_array.transpose()[0]

# parameters
time_window=300 #s durée d'extraction du signal pour le calcul des features
time_slide=60 #s glissement de la fenêtre 
freq_resampling=3.41 #Hz
method=tf.tf_crosspower

#statistics parameters
method_PRx=stats.pearsonr # method for correlation calculation

fs=200 #constant

ABP_time=np.arange(len(ABP_signal))/fs
time_tot=len(ABP_time)/fs # total time of aquisition

## creation of a file to write the output

outputfolder='../../output/'
if not os.path.exists(outputfolder):
    os.makedirs(outputfolder)
    
# Create output files
# it will be erased and rewriten at each call of the script
output_file=open(outputfolder+patientdir+study_name+'TFmag.txt', 'w')
output_file2=open(outputfolder+patientdir+study_name+'TFphase.txt', 'w')
output_log=open(outputfolder+patientdir+study_name+'TF_log.txt','w')
output_cross=open(outputfolder+patientdir+study_name+'ABP_ICP.txt','w')

#initialisation with empty lists 
failed_times=[]
nb_analysed_windows=0
    

t=time.process_time()
first=True
date = datetime.datetime.now()

# calculation of Bode parameters   
f_Pyx,dB_mag,phase,times=method(ABP_signal,ICP_signal,ABP_time,time_window,overlap=(time_window-time_slide))

#we count the number of analysed windows
nb_analysed_windows=len(times)  

# we want to select frequencies between 0 and 20 Hz
index_filter=np.where(f_Pyx<=20)[0]
freq_filter=f_Pyx[index_filter]
dB_mag_filter=dB_mag[index_filter,:]  
phase_filter=phase[index_filter,:]

# let's transpose the matrix to have times in line and frequencies in column
dB_mag_filter=dB_mag_filter.transpose() 
phase_filter=phase_filter.transpose()

#print to the output files
# if first time called
if first :
            
    output_file.write("#time/frequency\t" ) 
    output_file2.write("#time/frequency\t" )
    # write frequency line 
    for i in range(len(freq_filter)):
        output_file.write("%f\t" % freq_filter[i] )
        output_file2.write("%f\t" % freq_filter[i] )
    output_cross.write("#time\t")
    output_cross.write("Correlation\t")
    output_cross.write("pvalue\t")
    output_cross.write("CPP mean\t")
    output_cross.write("CPP std\t")
    output_cross.write("PRx\t")
    output_cross.write("PRx p-val\t")
    first=False
            
    #line change
    output_file.write("\n")
    output_file2.write("\n")
    output_cross.write("\n")
    
# write values calculated in the file  
# i:int    
for i in range(len(times)):  
    output_file.write("%f\t" % times[i])
    output_file2.write("%f\t" % times[i])
    #j:int
    for j in range(len(freq_filter)):
        output_file.write("%f\t" % dB_mag_filter[i,j])
        output_file2.write("%f\t" % phase_filter[i,j])
            
        
    #line change
    output_file.write("\n\n")
    output_file2.write("\n\n")
            

        
# ABP_ICP features calculation
ti=30 # start after 30 seconds
while ti<time_tot-time_window :
    #features calculation
    indices=da.indexseq(ti,time_window, ABP_time)
    time_avg=ti+time_window/2
    correlation,pvalue,cpp_mean,cpp_std=tf.ABP_ICP_features(ABP_signal[indices],ICP_signal[indices],method=method_PRx)
    prx,prx_pval=tf.PRx(ABP_time[indices],ABP_signal[indices],ICP_signal[indices],method_PRx)
    #write features in the file 
    output_cross.write("%f\t" % time_avg)
    output_cross.write("%.3e\t" % correlation)
    output_cross.write("%.3e\t" % pvalue)
    output_cross.write("%.3e\t" % cpp_mean)
    output_cross.write("%.3e\t" % cpp_std)
    output_cross.write("%.3e\t" % prx)
    output_cross.write("%.3e\t" % prx_pval)
    output_cross.write("\n")
    
    # go to next time window
    ti=ti+time_slide

#fill the analysis informations file
time_execution = time.process_time() -t  #calculate total  time of execution

output_log.write("date and time of the analysis : %s \n" % date.strftime("%H:%M:%S %d/%m/%Y ") )  
output_log.write("Choice of the window size: %s seconds\n" % time_window)
output_log.write("Choice of the peak detection method: %s \n" % method)
output_log.write("Total number of analysed windows: %s \n" % nb_analysed_windows)
output_log.write("Total time of execution: %s seconds \n" % time_execution )
output_log.write("Problematic times: %s \n" % failed_times)
output_log.write("Number of failed times: %s \n" % len(failed_times))

output_file.close()
output_file2.close()
output_log.close()
