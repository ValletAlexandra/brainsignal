# -*- coding: utf-8 -*-
"""
Created on Thu Jun  3 16:32:34 2021

@author: Hamon
"""
#import
 
import os
import sys
path=r'C:\Users\Hamon\Documents\Stage\brainsignal'
sys.path.append(path)
import brainsignal.datageneration as dg
import brainsignal.datanalysis as da
import brainsignal.HRVanalysis as ha
import brainsignal.detectionpeak as dp
import brainsignal.ICPanalysis as ia
import numpy as np
import scipy as sp
import glob
import pandas as pd
import seaborn as sns
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.stats import zscore
from scipy import signal
import time 
import datetime


#load data 
rep='../../data/'
patientdir='ID_3' #dossier du patient 
study_name='window5min'

#ABP data
file=glob.glob(rep+patientdir+'/WAVE_ABP.txt', recursive=True)[0]

data=pd.read_csv(file,header=None, decimal=',', delimiter='\n')
data_array=np.array(data.values)
ABP_signal=data_array.transpose()[0]


# parameters
time_window=300 #s durée d'extraction du signal pour le calcul des features
time_slide=60 #s glissement de la fenêtre 
freq_resampling=3.41 #Hz
method=dp.peakdetection4

fs=200 #constant

ABP_time=np.arange(len(ABP_signal))/fs
time_tot=len(ABP_time)/fs # total time of aquisition

## creation of a file to write the output

outputfolder='../../output/'
if not os.path.exists(outputfolder):
    os.makedirs(outputfolder)
    
# Create output files
# it will be erased and rewriten at each call of the script
output_file=open(outputfolder+patientdir+study_name+'ABP.txt', 'w')
output_log=open(outputfolder+patientdir+study_name+'ABP_log.txt','w')

#initialisation avec une liste vide 
failed_times=[]
nb_analysed_windows=0
    

ti=30 #int
t=time.process_time()
first=True
date = datetime.datetime.now()

# calculation of normalized time
df = pd.read_excel ('C:/Users/Hamon/Documents/Stage/data/times.xls')
line=np.where(df['Patient']==patientdir)[0][0]
time_norm =df['Initial time'][line]
t_norm=(time_norm.hour*3600+time_norm.minute*60+time_norm.second+ti+time_window/2)-23*3600

while ti<time_tot-time_window:
    
    #we count the number of analysed time windows
    nb_analysed_windows+=1
    
    indices=da.indexseq(ti,time_window, ABP_time)
    time_avg=ti+time_window/2
    
    #ABP signal features
    correct=np.where((ABP_signal[indices]<250) & (ABP_signal[indices]>10))[0]
    # threshold to take of outliers for mean and std calculation
    mean_ABP=np.mean(ABP_signal[indices][correct])
    std_ABP=np.std(ABP_signal[indices][correct])
    
    # rr intervals 
    time_rr,rr_corrected,hr,power_hr,n, power_integrated=ha.rr_intervals(ABP_signal[indices],ABP_time[indices],method)
    
    
    # calculation of systolic pressure and diastolic pressure features in the time window
    mean_psys,std_psys,max_psys,min_psys,psys_median,outliers_psys,idx_sys,n=ha.psys(ABP_signal[indices], hr)
    mean_pdia,std_pdia,max_pdia,min_pdia,outliers_pdia,idx_dia=ha.pdia(ABP_time[indices], ABP_signal[indices], hr)
    
    # if there is a problem in the time window we add it to failed_times list and go to the next window    
    if (np.isnan(mean_psys) or np.isnan(mean_pdia) or  mean_psys-mean_pdia<=0 ):
        failed_times.append(ti) #we add the time to the list of failed times          
        ti=ti+time_slide 
        t_norm=t_norm+time_slide
    
    # else we continue the calculation of features        
    else :
        # amplitude calculation 
        amplitude, time_amplitude, pdia=ha.amplitude_ABP(ABP_time[indices],ABP_signal[indices],idx_sys,idx_dia,hr)
        #features calculation in time domain 
        td=ha.timedomain(rr_corrected)
    
        #frequency domain 
        ## rr_corrected interpolation 
        spantime=dg.generate_time(time_window-1,freq_resampling,ti)
        rr_corrected=rr_corrected-np.mean(rr_corrected) # we remove the mean 
        rr_interpolated=InterpolatedUnivariateSpline(time_rr,rr_corrected)
        fxx,pxx =da.periodogram(rr_interpolated(spantime),spantime)
        ## features calculation
        fd=ha.frequency_domain(fxx, pxx)
    

        #print to the output file
    
        # if first time called, write the item names
        if first :
            output_file.write("#time\t" ) 
            output_file.write("Number of corrected RRi\t" )
            output_file.write("Heart frequency (Hz)\t")
            output_file.write("Power hr (ms2)\t")
            output_file.write("Power hr integrated (ms2)\t")
            output_file.write("Mean ABP (mmHg)\t")
            output_file.write("STD ABP (mmHg)\t")
            output_file.write("Amplitude mean (mmHg)\t")
            output_file.write("Amplitude std (mmHg)\t")
            output_file.write("Psys (mmHg)\t")
            output_file.write("Pdia (mmHg)\t")
            output_file.write("Psys STD (mmHg)\t")
            output_file.write("Pdia STD (mmHg)\t")
            output_file.write("Psys max (mmHg)\t")
            output_file.write("Pdia max (mmHg)\t")
            output_file.write("Psys min (mmHg)\t")
            output_file.write("Pdia min (mmHg)\t")
            output_file.write("Psys median (mmHg)\t")
            output_file.write("Psys outliers\t")
            output_file.write("Pdia outliers\t")
            output_file.write("Psys outliers normalized\t")
            for item in td.items(): #features in time domain 
                output_file.write("%s\t" % item[0])
                    
            for item in fd.items(): #features in frequecy domain 
                output_file.write("%s\t" % item[0]) 
                
            output_file.write("time normalized\t" )
            first=False
            
            #line change
            output_file.write("\n")
            
        #write features calculated in the file
        output_file.write("%f\t" % time_avg)
        output_file.write("%f\t" % n)
        output_file.write("%.3e\t" % hr)
        output_file.write("%.3e\t" % power_hr)
        output_file.write("%.3e\t" % power_integrated)
        output_file.write("%.3e\t" % mean_ABP)
        output_file.write("%.3e\t" % std_ABP)
        output_file.write("%.3e\t" % np.mean(amplitude))
        output_file.write("%.3e\t" % np.std(amplitude))
        output_file.write("%.3e\t" % mean_psys)
        output_file.write("%.3e\t" % mean_pdia)
        output_file.write("%.3e\t" % std_psys)
        output_file.write("%.3e\t" % std_pdia)
        output_file.write("%.3e\t" % max_psys)
        output_file.write("%.3e\t" % max_pdia)
        output_file.write("%.3e\t" % min_psys)
        output_file.write("%.3e\t" % min_pdia)
        output_file.write("%.3e\t" % psys_median)
        output_file.write("%f\t" % outliers_psys)
        output_file.write("%f\t" % outliers_pdia)
        output_file.write("%f\t" % n)


        
        for item in td.items(): #features in time domain 
            output_file.write("%.3e\t" % item[1])
                
        for item in fd.items(): #features in frequecy domain 
            output_file.write("%.3e\t" % item[1])
            
        output_file.write("%f\t" % t_norm)
        #line change
        output_file.write("\n")

            
        #incrementation to go to next time window
        ti=ti+time_slide
            
        t_norm=t_norm+time_slide

#fill the analysis informations file
time_execution = time.process_time() -t  #calculate total  time of execution

output_log.write("date and time of the analysis : %s \n" % date.strftime("%H:%M:%S %d/%m/%Y ") )  
output_log.write("Choice of the window size: %s seconds\n" % time_window)
output_log.write("Choice of the peak detection method: %s \n" % method)
output_log.write("Total number of analysed windows: %s \n" % nb_analysed_windows)
output_log.write("Total time of execution: %s seconds \n" % time_execution )
output_log.write("Problematic times: %s \n" % failed_times)
output_log.write("Number of failed times: %s \n" % len(failed_times))

output_file.close()
output_log.close()
    
