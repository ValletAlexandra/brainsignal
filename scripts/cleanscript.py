# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 2021

@author: Hamon
"""

#import
 
import os
import sys
path=r'C:\Users\Hamon\Documents\Stage\brainsignal'
sys.path.append(path)
import brainsignal.datageneration as dg
import brainsignal.datanalysis as da
import brainsignal.HRVanalysis as ha
import brainsignal.detectionpeak as dp
import brainsignal.ICPanalysis as ia
import numpy as np
import scipy as sp
import glob
import pandas as pd
import seaborn as sns
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.stats import zscore
from scipy import stats
from scipy import signal
import time 
import datetime


#data loading 
rep='../../output/'
patientdir='ID_2' #dossier du patient 
study_name='window5min'
file=glob.glob(rep+patientdir+study_name+'ABP.txt', recursive=True)[0]
data=pd.read_csv(file, decimal='.', delimiter='\t')

#parameters 
method=data['Power hr (ms2)']
threshold=np.max(method)*5/100

method2=np.abs(data['Heart frequency (Hz)']-(1/(data['Mean RR (ms)']*1e-3)))/data['Heart frequency (Hz)']*100
threshold2=5
#create new file 
## creation of a file to write the output
outputfolder='../../output/'
if not os.path.exists(outputfolder):
    os.makedirs(outputfolder)
output_file=open(outputfolder+patientdir+study_name+'ABP_clean.txt', 'w')
first=True

# load variables
variables=data.columns

# search time when signal is not good 
index=np.where(method>threshold)[0]
index2=np.where(method2<threshold2)[0]

#replace outliers by np.nan    
data.iloc[index]=np.nan
data.iloc[index2]=np.nan
# write data cleaned in the output file
## write features name if first
if first:
    for var in variables:
       output_file.write("%s\t" % var ) 
    first=False 
    output_file.write("\n")
## write features values
for i in range(len(data)):
    for var in variables:
        if np.isnan(data[var][i]):
            output_file.write("%f\t\t" % data[var][i])
        else:
            output_file.write("%f\t" % data[var][i])
    output_file.write("\n")

# close the outout file    
output_file.close()
