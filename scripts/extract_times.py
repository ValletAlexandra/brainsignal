from re import sub
import pandas as pd
import re
import re

import os
import sys

"""
This script extracts the time-info from a datafolder.
Should be ran inside the "sripts"-folder. 
The datafolder should be one layer above. 
If not, the paths to the files/folder needs to be changed.
"""

module_path = os.path.abspath(os.path.join('../data'))
if module_path not in sys.path:
    sys.path.append(module_path)

column_names = ['PatentID', 'Type', 'Date', 'Time', 'TypeID']
df = pd.DataFrame(columns=column_names)

# os.walk recursively visits all subfolders with their files
for path, subdirs, files in os.walk(module_path):
    path_list = path.split('/')
    if path_list[-1].startswith('PatID'):                     # we want the files that starts with PatID
        patientid = int(re.findall(r'\d+', path_list[-1])[0]) # use regex to extract the PatID
        for idx, s in enumerate(files):                       # loop through all files in patient folder
            slist = s.split('_') 
            l = [patientid, slist[1], slist[2], slist[3], slist[4]] # extract data
            df2 = pd.DataFrame([l], columns=column_names)     # put data inside a dateframe
            df = pd.concat([df,df2], ignore_index=True)

df.to_csv('../data/times.csv')