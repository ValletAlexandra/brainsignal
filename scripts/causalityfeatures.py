# -*- coding: utf-8 -*-
#import
import os
import sys
import re
from xmlrpc.client import Boolean

# Run the program from the script folder. 
# The lines under is needed to import functionality from other folders. 
path = os.path.abspath(os.path.join('..'))
sys.path.append(path)
 
import brainsignal.datageneration as dg
import brainsignal.datanalysis as da
import brainsignal.HRVanalysis as ha
import brainsignal.detectionpeak as dp
import brainsignal.ICPanalysis as ia
import brainsignal.causality as ca
import numpy as np
import scipy as sp
import glob
import pandas as pd
import seaborn as sns
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.stats import zscore
from scipy import signal
import time 
import datetime
import argparse

parser = argparse.ArgumentParser(description='Calculate causality data for one patient.')
parser.add_argument('-dd', '--data_dir', default='../data/', help='Directory to the data')
parser.add_argument('-sd','--save_dir', default='saved_statistics/', help='Folder to save statistics data')
parser.add_argument('-pd', '--patient_dir', default='PatID 3_Txt Files', help='Folder to patientdata')
parser.add_argument('-sn', '--study_name', default='', help='Special name for study')
parser.add_argument('-tw', '--time_window', default=300, type=int, help='Time window')
parser.add_argument('-ts', '--time_slide', default=60, type=int, help='Time slide')
parser.add_argument('-it', '--initial_time', default=30, type=int, help='Time slide')
parser.add_argument('-s', '--standardize', default=False, type=bool, help='Standardize')
parser.add_argument('-rs', '--resample', default=False, type=bool, help='Resampled frequency')
parser.add_argument('-rf', '--resampling_frequency', default=200, type=int, help='Resampled frequency')

args = parser.parse_args()
if '\n' in args.patient_dir: args.patient_dir = args.patient_dir.replace('\n', ' ')

# save names and create folders
data_dir = args.data_dir
save_dir = args.save_dir
patient_dir = args.patient_dir 
study_name = '_' + args.study_name if args.study_name else args.study_name # Add underscore before study name.
patientid = int(re.findall(r'\d+', patient_dir)[0]) # use regex to extract the PatID
time_window = args.time_window
time_slide = args.time_slide
initial_time = args.initial_time
std = args.standardize
resample = args.resample
resampling_frequency = args.resampling_frequency
study_time = datetime.date.today().strftime('%Y%m%d_') + time.strftime('%H%M%S')
save_name = f'Pat_{patientid}_{study_time}_window_{time_window}_slide_{time_slide}{study_name}'

output_folder = data_dir + save_dir + save_name
if not os.path.exists(output_folder):
    os.makedirs(output_folder)
    
# Create output files
output_file=open(output_folder + '/' + save_name + '_Gc_data.txt', 'w')
output_log=open(output_folder + '/' + save_name + '_Gc_log.txt','w')

# ABP data
file_ABP=glob.glob(data_dir + patient_dir + '/WAVE_ABP_*', recursive=True)[0]
data_ABP=pd.read_csv(file_ABP,header=None, decimal=',', delimiter=';')
data_array_ABP=np.array(data_ABP.values)
ABP_signal=data_array_ABP.transpose()[0]

# ICP data
file_IPC = glob.glob(data_dir + patient_dir +'/WAVE_ICP*', recursive=True)[0]
data_IPC=pd.read_csv(file_IPC,header=None, decimal=',', delimiter=';')
data_array_IPC=np.array(data_IPC.values)
ICP_signal=data_array_IPC.transpose()[0]

fs=200 #constant

ABP_time=np.arange(len(ABP_signal))/fs
time_tot=len(ABP_time)/fs # total time of aquisition
ti = 30

# normalization of time
df = pd.read_excel('../data/times.xls')
patient_str = 'ID' + str(patientid)
line=np.where(df['Patient']==patient_str)[0][0]
time_norm =df['Initial time'][line]
t_norm=(time_norm.hour*3600+time_norm.minute*60+time_norm.second+ti+time_window/2)-23*3600

t=time.process_time()
first=True

# Counter for the number of windows that have been looped through.
counter = 0
print('Start of patient: ', patientid)

while ti<time_tot-time_window:
    indices=da.indexseq(ti,time_window, ABP_time)
    time_avg=ti+time_window/2

    gc_dict = ca.causality_statistics(ABP_time[indices], ICP_signal[indices], ABP_signal[indices], standardize=std, resample=resample, resampling_frequency=resampling_frequency)

    # If first time called, write the item names.
    if first :
        output_file.write("#time\t" ) 
        output_file.write("time normalized\t" )
        output_file.write("y_to_x_lag_min\t")
        output_file.write("y_to_x_lag_max\t")
        output_file.write("y_to_x_lag_median\t")
        output_file.write("y_to_x_lag_std\t")
        output_file.write("y_to_x_F_min\t")
        output_file.write("y_to_x_F_max\t")
        output_file.write("y_to_x_F_median\t")
        output_file.write("y_to_x_F_std\t")
        output_file.write("y_to_x_p_min\t")
        output_file.write("y_to_x_p_max\t")
        output_file.write("y_to_x_p_median\t")
        output_file.write("y_to_x_p_std\t")
        output_file.write("y_to_x_percentage_nans\t")
        output_file.write("x_to_y_lag_min\t")
        output_file.write("x_to_y_lag_max\t")
        output_file.write("x_to_y_lag_median\t")
        output_file.write("x_to_y_lag_std\t")
        output_file.write("x_to_y_F_min\t")
        output_file.write("x_to_y_F_max\t")
        output_file.write("x_to_y_F_median\t")
        output_file.write("x_to_y_F_std\t")
        output_file.write("x_to_y_p_min\t")
        output_file.write("x_to_y_p_max\t")
        output_file.write("x_to_y_p_median\t")
        output_file.write("x_to_y_p_std\t")
        output_file.write("x_to_y_percentage_nans\t")
        output_file.write("\n")
                
        first=False
            
    # Write features calculated in the file.
    output_file.write(f"{time_avg:f}\t")
    output_file.write(f"{t_norm:f}\t")
    output_file.write(f"{gc_dict['y_to_x']['lag']['min']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['lag']['max']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['lag']['median']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['lag']['std']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['F']['min']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['F']['max']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['F']['median']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['F']['std']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['p']['min']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['p']['max']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['p']['median']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['p']['std']:.3e}\t")
    output_file.write(f"{gc_dict['y_to_x']['percentage_nans']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['lag']['min']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['lag']['max']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['lag']['median']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['lag']['std']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['F']['min']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['F']['max']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['F']['median']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['F']['std']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['p']['min']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['p']['max']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['p']['median']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['p']['std']:.3e}\t")
    output_file.write(f"{gc_dict['x_to_y']['percentage_nans']:.3e}\t")
    output_file.write("\n")

    # Incrementation to go to next time window.
    ti=ti+time_slide
    t_norm=t_norm+time_slide
        
    print(patientid, counter, ti)
    counter += 1

#fill the analysis informations file
time_execution = time.process_time() -t  #calculate total  time of execution
output_log.write("date and time of the analysis : %s \n" % datetime.datetime.now().strftime("%H:%M:%S %d/%m/%Y ") )  
output_log.write("Choice of the window size: %s seconds\n" % time_window)
output_log.write("Total time of execution: %s seconds \n" % time_execution )

output_file.close()
output_log.close()
    
print('End of patient: ', patientid)