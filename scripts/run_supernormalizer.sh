#!/bin/sh


# First, the patient data must be in a folder called 'data' at the same level as the scripts-folder.
# This script runs through all the patients and calls the 
# supernormalizer_script.py file.
# Should be called inside the scripts folder.

# Optional arguments to add
# Remember to add them in the call below
#data_dir
#statistics_dir
#patient_dir
#study_name
#time_window
#time_slide
#initial_time
#number_of_points

for f in ../data/PatID* ; do
    patient_dir=$(basename $f)
    python supernormalizer_script.py --patient_dir "$patient_dir"
done