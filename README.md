# Intracranial pressure and arterial pressure signal analysis

BRAINSIGNAL is a Python code package designed for the processing of intracranial and arterial pressure signals, specifically for full-night measurements obtained from patients with normal pressure hydrocephalus. The package includes preprocessing scripts to clean data and remove artifacts, as well as algorithms to rescale pulsations so that each cardiac cycle aligns on a uniform time scale.

The package enables the extraction of quantitative descriptors for heart rate variability (HRV) from invasive arterial pressure signals. It computes both linear and non-linear markers, facilitating the characterization of the balance between sympathetic and parasympathetic activity. A clustering approach is employed to define similar physiological states during sleep.

In addition, the package analyzes the intracranial pressure signal to provide insights into vascular tone, intracranial pulsatility, average pressure, and respiratory frequency. These metrics are used in conjunction with HRV metrics for a comprehensive understanding of the subject's physiological state.

Lastly, a Granger causality approach is utilized to assess the relationships between these various markers.
